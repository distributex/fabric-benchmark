var Fabric_Client = require('fabric-client');
var path = require('path');
var util = require('util');
var os = require('os');


async function sleep(timeout_ms) {
	return new Promise((resolve, reject) => {
		setTimeout(() => {
			resolve(timeout_ms);
		}, timeout_ms);
	});
}

class Chain {
	async init() {
		this.fabric_client = new Fabric_Client();
		// setup the fabric network
		this.channel = this.fabric_client.newChannel('mychannel');
		this.peer = this.fabric_client.newPeer('grpc://localhost:7051');
		this.channel.addPeer(this.peer);
		this.channel.addPeer(this.fabric_client.newPeer('grpc://localhost:7251'));
		
		this.order = this.fabric_client.newOrderer('grpc://localhost:7050')
		this.channel.addOrderer(this.order);
		var store_path = path.join(__dirname, 'hfc-key-store');
		console.error('Store path:'+store_path);
	
		// create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
		this.state_store = await Fabric_Client.newDefaultKeyValueStore({ path: store_path});
		// assign the store to the fabric client
		this.fabric_client.setStateStore(this.state_store);
		var crypto_suite = Fabric_Client.newCryptoSuite();
		// use the same location for the state store (where the users' certificate are kept)
		// and the crypto store (where the users' keys are kept)
		var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
		crypto_suite.setCryptoKeyStore(crypto_store);
		this.fabric_client.setCryptoSuite(crypto_suite);
	
		// get the enrolled user from persistence, this user will sign all requests
		this.user_from_store = await this.fabric_client.getUserContext('user1', true);
		if (this.user_from_store && this.user_from_store.isEnrolled()) {
			console.error('Successfully loaded user1 from persistence');
		} else {
			throw new Error('Failed to get user1.... run registerUser.js');
		}

		// get an eventhub once the fabric client has a user assigned. The user
		// is required bacause the event registration must be signed
		this.event_hub = this.channel.newChannelEventHub(this.peer);
		this.event_hub.connect();
		return true;
	}

	async sendTransaction() {
		// get a transaction id object based on the current user assigned to fabric client
		const tx_id = this.fabric_client.newTransactionID();
		console.error("Assigning transaction_id: ", tx_id._transaction_id);
	
		// createCar chaincode function - requires 5 args, ex: args: ['CAR12', 'Honda', 'Accord', 'Black', 'Tom'],
		// changeCarOwner chaincode function - requires 2 args , ex: args: ['CAR10', 'Dave'],
		// must send the proposal to endorsing peers
		var request = {
			//targets: let default to the peer assigned to the client
			chaincodeId: 'fabcar',
			fcn: 'createCar',
			args: ['CAR10', 'Chevy', 'Volt', 'Red', 'Nick'],
			chainId: 'mychannel',
			txId: tx_id
		};
	
		// send the transaction proposal to the peers
		
		let results;
		let proposalResponses;
		let isProposalGood;
		
		const start_ms = Date.now();

		do {
			results = await this.channel.sendTransactionProposal(request);
			proposalResponses = results[0];
			isProposalGood = (proposalResponses && proposalResponses[0].response && proposalResponses[0].response.status === 200);
			if (!isProposalGood) {
				await sleep(500);
			}
		} while (!isProposalGood);

		var proposal = results[1];
		if (isProposalGood) {
			console.error(util.format(
				'Successfully sent Proposal and received ProposalResponse: Status - %s, message - "%s"',
				proposalResponses[0].response.status, proposalResponses[0].response.message));
	
			// build up the request for the orderer to have the transaction committed
			var request = {
				proposalResponses: proposalResponses,
				proposal: proposal
			};
	
			// set the transaction listener and set a timeout of 30 sec
			// if the transaction did not get committed within the timeout period,
			// report a TIMEOUT status
			var transaction_id_string = tx_id.getTransactionID(); //Get the transaction ID string to be used by the event processing
			var promises = [];
	
			var sendPromise = this.channel.sendTransaction(request);
			promises.push(sendPromise); //we want the send transaction first, so that we know where to check status
	
			// using resolve the promise so that result status may be processed
			// under the then clause rather than having the catch clause process
			// the status
			let txPromise = new Promise((resolve, reject) => {
				let handle = setTimeout(() => {
					resolve({event_status : 'TIMEOUT'}); //we could use reject(new Error('Trnasaction did not complete within 30 seconds'));
				}, 60000);
				this.event_hub.registerTxEvent(transaction_id_string, (tx, code) => {
					// this is the callback for transaction event status
					// first some clean up of event listener
					clearTimeout(handle);
					this.event_hub.unregisterTxEvent(transaction_id_string);
	
					// now let the application know what happened
					var return_status = {event_status : code, tx_id : transaction_id_string};
					if (code !== 'VALID') {
						console.error('The transaction was invalid, code = ' + code);
						resolve(return_status); // we could use reject(new Error('Problem with the tranaction, event status ::'+code));
					} else {
						console.error('The transaction has been committed on peer ' + this.event_hub.getPeerAddr());
						resolve(return_status);
					}
				}, (err) => {
					//this is the callback if something goes wrong with the event registration or processing
					reject(new Error('There was a problem with the eventhub ::'+err));
				});
			});
			promises.push(txPromise);
	
			results = await Promise.all(promises);
		}
		// check the results in the order the promises were added to the promise all list
		if (results && results[0] && results[0].status === 'SUCCESS') {
			console.error('Successfully sent transaction', transaction_id_string, 'to the orderer.');
		} else {
			console.error('Failed to order the transaction. Error code: ' + results[0].status);
		}
	
		if(results && results[1] && results[1].event_status === 'VALID') {
			console.error('Successfully committed the change to the ledger by the peer');
		} else {
			console.error('Transaction failed to be committed to the ledger due to ::'+results[1].event_status);
		}

		return Date.now() - start_ms;
	}
}

async function main() {
	const TXS = 1024;
	const chain = new Chain();
	await chain.init();
	const start_ms = Date.now();
	let promises = [];
	for (let i = 0; i < TXS; i++) {
		promises.push(chain.sendTransaction());
	}

	const latencies = await Promise.all(promises);
	const max_latency = Math.max(latencies);
	const min_latency = Math.min(latencies);
	const duration = (Date.now() - start_ms) / 1000;
	const tx_per_second = TXS / duration;
	
	console.log('Execution time is', duration, ' seconds, tx/s is ', tx_per_second, '. ', TXS, 'transactions were commited. Max/min latency:', max_latency, min_latency);
	process.exit(0);
}


main().then(console.error).catch(console.error);
